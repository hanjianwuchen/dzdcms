<?php

// +----------------------------------------------------------------------
// | Yzncms [ 御宅男工作室 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://yzncms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 御宅男 <530765310@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 前台控制模块
// +----------------------------------------------------------------------

namespace app\common\controller;

use think\facade\Config;
use app\common\model\Site;
use think\facade\Cookie;
use think\facade\Hook;

class Homebase extends Base
{
    protected $modulename     = null;
    protected $controllername = null;
    protected $actionname     = null;

    //初始化
    protected function initialize()
    {
        //移除HTML标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        parent::initialize();
        $this->modulename     = $this->request->module();
        $this->controllername = parse_name($this->request->controller());
        $this->actionname     = strtolower($this->request->action());
        // dzdcms
        $config = get_addon_config('cms');
        $domain = isset($config['domain']) ? $config['domain'] : 1;
        $sites  = allSite();
        //前端显示的所有在线的站点
        $allSite   = [];
        foreach ($sites as $v) {
            if ($v['close'] == 1) {
                $allSite[] = $v;
            }
        }
        //语言设定
        $curr_domain    = $_SERVER['HTTP_HOST'];
        $this->siteId = getSiteId();
        $mark = $sites[$this->siteId]['mark'];
        if ($domain && Cookie::set('var', '')) {
            Cookie::set('var', $mark);
            Cookie::set('site_id', $this->siteId);
            setLang($mark);
        } else {
            Cookie::set('var', $mark);
            Cookie::set('site_id', $this->siteId);
        }
        if ($curr_domain !== $sites[$this->siteId]['domain']) {
            header('Location:'.$_SERVER['REQUEST_SCHEME'].'://'.$sites[$this->siteId]['domain']);
            exit;
        }
        //$siteName 虚拟站点显示自己的站点名称 独立站不显示
        if (getSite('alone') != 1) {
            $siteId   = 1;
            $siteName = getSite('name');
        } else {
            $siteId   = getSite('id');
            $siteName = '';
        }
        // dzdcms end

        $site   = Config::get("site.");
        $config = [
            'modulename'     => $this->modulename,
            'controllername' => $this->controllername,
            'actionname'     => $this->actionname,
        ];
        //监听插件传入的变量
        $site = array_merge($site, $config, Hook::listen("config_init")[0] ?? []);
        $this->assign([
            'site'     => $site,
            'domain'   => $domain,
            'allSite'  => $allSite,
            'siteName' => $siteName,
            'siteId'   => $siteId,
        ]);
    }

    /**
     * 渲染配置信息
     * @param mixed $name  键名或数组
     * @param mixed $value 值
     */
    protected function assignconfig($name, $value = '')
    {
        $this->view->site = array_merge($this->view->site ? $this->view->site : [], is_array($name) ? $name : [$name => $value]);
    }

    protected function fetch($template = '', $vars = [], $config = [], $renderContent = false)
    {
        //$Theme    = empty(Config::get('site.theme')) ? 'default' : Config::get('site.theme');
        //$viewPath = TEMPLATE_PATH . $Theme . DS . $this->modulename . DS;
        $cmsConfig    = get_addon_config('cms');
        $siteTheme    = getSite('template');
        $wapTheme     = $cmsConfig['wap_template'];
        $Theme        = empty($siteTheme) ? 'default' : $siteTheme;

        if ($wapTheme && ($this->request->isMobile() && $this->request->module() == "cms")) {
            $viewPath = TEMPLATE_PATH . $Theme . DIRECTORY_SEPARATOR . 'wap' . DIRECTORY_SEPARATOR;
        } else {
            $viewPath = TEMPLATE_PATH . $Theme . DS . $this->modulename . DS;
        }

        $tempPath     = TEMPLATE_PATH . $Theme . DS . $this->modulename . DS . ($this->modulename === 'index' ? $this->controllername : '') . DS;
        $templateFile = $tempPath . trim($template, '/') . '.' . Config::get('template.view_suffix');
        if ('default' !== $Theme && !is_file($templateFile)) {
            $viewPath = TEMPLATE_PATH . 'default' . DS . $this->modulename . DS;
        }

        $this->view->config('view_path', $viewPath);
        return $this->view->fetch($template, $vars, $config, $renderContent);
    }
}
